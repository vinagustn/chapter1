import java.util.Scanner;

public class Challenge1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int inputMain, pilih;
        double sisi, panjang, lebar, tinggi, jari_jari,alas;

        do{
            menu();
            inputMain = input.nextInt();

            boolean ulang = true;
            while(ulang){
                if(inputMain == 0){
                    System.exit(0);
                }else if(inputMain == 1){
                    secondMenu();
                    pilih = input.nextInt();

                    if(pilih != 0){
                        switch(pilih){
                            case 1:
                                System.out.println("\nKamu pilih Persegi");
                                System.out.print("Masukkan sisi : ");
                                sisi = input.nextDouble();
                                persegi(sisi);
                                break;
                            case 2:
                                System.out.println("\nKamu pilih Lingkaran");
                                System.out.print("Masukkan jari-jari : ");
                                jari_jari = input.nextDouble();
                                lingkaran(jari_jari);
                                break;
                            case 3:
                                System.out.println("\nKamu pilih Segitiga");
                                System.out.print("Masukkan alas   : ");
                                alas = input.nextDouble();
                                System.out.print("Masukkan tinggi : ");
                                tinggi = input.nextDouble();
                                segitiga(alas, tinggi);
                                break;
                            case 4:
                                System.out.println("\nKamu pilih Persegi Panjang");
                                System.out.print("Masukkan panjang : ");
                                panjang = input.nextDouble();
                                System.out.print("Masukkan lebar   : ");
                                lebar = input.nextDouble();
                                persegiPanjang(panjang, lebar);
                                break;
                            default:
                                menuKosong();
                                break;
                        }
                        back();
                        input.next();
                    }else ulang = false;
                }else if(inputMain == 2){
                    thirdMenu();
                    pilih = input.nextInt();

                    if (pilih != 0){
                        switch (pilih){
                            case 1:
                                System.out.println("\nKamu pilih Kubus");
                                System.out.print("Masukkan sisi : ");
                                sisi = input.nextDouble();
                                kubus(sisi);
                                break;
                            case 2:
                                System.out.println("\nKamu pilih Balok");
                                System.out.print("Masukkan panjang : ");
                                panjang = input.nextDouble();
                                System.out.print("Masukkan lebar   : ");
                                lebar = input.nextDouble();
                                System.out.print("Masukkan tinggi  : ");
                                tinggi = input.nextDouble();
                                balok(panjang, lebar, tinggi);
                                break;
                            case 3:
                                System.out.println("\nKamu pilih Tabung");
                                System.out.print("Masukkan jari-jari : ");
                                jari_jari = input.nextDouble();
                                System.out.print("Masukkan tinggi    : ");
                                tinggi = input.nextDouble();
                                tabung(jari_jari, tinggi);
                                break;
                            default:
                                menuKosong();
                                break;
                        }
                        back();
                        input.next();
                    }else ulang = false;
                }else{
                    menuKosong();
                    back();
                    input.next();
                    ulang = false;
                }
            }
        }while(inputMain != 0);
        input.close();
    }

    static void menu(){
        System.out.println("---------------------------------");
        System.out.println("KALKULATOR HITUNG LUAS DAN VOLUME");
        System.out.println("---------------------------------");
        System.out.println("Menu : ");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume Bangun");
        System.out.println("0. Exit Aplikasi");

        System.out.print("\nSilahkan Masukkan Menu (0-2) : ");
    }
    static void secondMenu(){
        System.out.println("\nHitung Luas yang mana guys?");

        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Back");

        System.out.print("\nPilih yang mana (0-4) : ");
    }
    static void persegi(double s){
        double hasil = Math.pow(s,2);
        System.out.println("Luas Persegi : "+hasil);
    }
    static void lingkaran(double r){
        double hasil = 3.14d * Math.pow(r,2);
        System.out.println("Luas Lingkaran : "+hasil);
    }
    static void segitiga(double a, double t){
        double hasil = (a * t)/2;
        System.out.println("Luas Segitiga : "+hasil);
    }
    static void persegiPanjang(double p, double l){
        double hasil = p * l;
        System.out.println("Luas Persegi Panjang : "+hasil);
    }
    static void thirdMenu(){
        System.out.println("\nMenu hitung Volume, mau yang mana?");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Back");

        System.out.print("\nMasukkan pilihan (0-3) : ");
    }
    static void kubus(double s){
        double hasil = Math.pow(s,3);
        System.out.println("Volume Kubus : "+hasil);
    }
    static void balok(double p, double l, double t){
        double hasil = p * l * t;
        System.out.println("Volume Balok : "+hasil);
    }
    static void tabung(double r, double t){
        double hasil = 3.14 * Math.pow(r,2) * t;
        System.out.println("Volume Tabung : "+hasil);
    }
    static void menuKosong(){
        System.out.println("\nKamu ada-ada aja sih!");
        System.out.println("Menu yang kamu pilih gaada nih");
    }
    static void back(){
        System.out.print("\nKembali? ");
        System.out.println("Ketik apa aja lalu tekan enter ya");
    }

}